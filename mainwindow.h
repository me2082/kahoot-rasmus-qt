#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include  <QPropertyAnimation>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow (QWidget *parent = nullptr, QString userName = "");

    ~MainWindow ();

    //MainWindow(QWidget *parent = nullptr);
    //~MainWindow();


private slots:

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void updateQuestion();

    void wrongAnswer();

    void rightAnswer();

    void updateQuestionsMethod();

private:
    Ui::MainWindow *ui;

    MainWindow*mainwindow= nullptr;

    QPropertyAnimation *anim1;

};
#endif // MAINWINDOW_H
