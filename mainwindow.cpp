#include "userinfo.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QMediaPlayer>
#include <QAudioOutput>




//MainWindow::MainWindow(QWidget *parent)
MainWindow::MainWindow (QWidget *parent, QString userName)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->menuUser->setTitle(userName);
    updateQuestion();
    anim1 = new QPropertyAnimation(ui->label_2, "geometry");
}

MainWindow::~MainWindow()
{
    delete ui;
}

float score = 0;

int questionNum = -1;

QString questions[6] = {"Which is the First Pokemon?","Which one is a berry?","What family does the Potato plant belong to?","What programming Language is the most enegy efficient?","How many strings is there on a 4 string bass", "Well Done... or not you got a score of: " + QString::number(score) };

QString button1[6] = {"Bulbasaur","Strawberry","Ornithorhynchidae family (Monotremata) ","C","4",""};
bool isCorrect1[6] = {false,false,false,true,true,false};

QString button2[6] = {"Rhydon","Bananas","amaranth family (Amaranthaceae)","Rust","6",""};
bool isCorrect2[6] = {true,true,false,false,false,false};

QString button3[6] = {"Mew","Tomato","nightshade family (Solanaceae)","C++","8",""};
bool isCorrect3[6] = {false,true,true,false,false,false};

QString button4[6] = {"Arceus","Pumpkin","aster family (Asteraceae)","Perl","5",""};
bool isCorrect4[6] = {false,true,false,false,false,false};

QString imageLocation[6] {":/Images/Images/1.png",":/Images/Images/2.png",":/Images/Images/3.png",":/Images/Images/4.png",":/Images/Images/5.png",":/Images/Images/Correct.png"};

void MainWindow::updateQuestion()
{
    // Play audio
        QMediaPlayer *player = new QMediaPlayer;
        QAudioOutput *audioOutput = new QAudioOutput;
        player->setAudioOutput(audioOutput);
        player->setSource(QUrl::fromLocalFile("qrc:/Sound/Sound/Waiting.mp3"));
        audioOutput->setVolume(1);
        player->play();
    questionNum++;

    if(questionNum == 5)
    {
        ui->pushButton->hide();
        ui->pushButton_2->hide();
        ui->pushButton_3->hide();
        ui->pushButton_4->hide();

        //it dident want to grab the score so now we are doing it here
        questions[5] ="Well Done... or not you got a score of: " + QString::number(score);
    }
    else
    {
        ui->pushButton->show();
        ui->pushButton_2->show();
        ui->pushButton_3->show();
        ui->pushButton_4->show();
    }

    ui->label->setText(questions[questionNum]);

    ui->label_2->setPixmap(QPixmap(imageLocation[questionNum]));

    ui->pushButton->setText(button1[questionNum]);
    ui->pushButton_2->setText(button2[questionNum]);
    ui->pushButton_3->setText(button3[questionNum]);
    ui->pushButton_4->setText(button4[questionNum]);


}

void MainWindow::on_pushButton_clicked()
{
    if(isCorrect1[questionNum] == true)
    {
        //Answer is correct
        rightAnswer();
    }
    else
    {
        //Answer is false
        wrongAnswer();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    if(isCorrect2[questionNum] == true)
    {
        //Answer is correct
        rightAnswer();
    }
    else
    {
        //Answer is false
        wrongAnswer();
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    if(isCorrect3[questionNum] == true)
    {
        //Answer is correct
        rightAnswer();
    }
    else
    {
        //Answer is false
        wrongAnswer();
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    if(isCorrect4[questionNum] == true)
    {
        //Answer is correct
        rightAnswer();
    }
    else
    {
        //Answer is false
        wrongAnswer();
    }
}

void MainWindow::wrongAnswer()
{
    //Image Change

    ui->label->setText("WRONG");
    ui->label_2->setPixmap(QPixmap(":/Images/Images/Wrong.png"));
    ui->pushButton->hide();
    ui->pushButton_2->hide();
    ui->pushButton_3->hide();
    ui->pushButton_4->hide();


    anim1->setDuration(10000);
    anim1->setStartValue(ui->label_2->geometry());
    anim1->setEndValue(QRect(ui->label_2->geometry().x(),ui->label_2->geometry().y(),ui->label_2->geometry().width()*2, ui->label_2->geometry().height()*2));
    anim1->start();

    // Play audio
        QMediaPlayer *player = new QMediaPlayer;
        QAudioOutput *audioOutput = new QAudioOutput;
        player->setAudioOutput(audioOutput);
        player->setSource(QUrl::fromLocalFile("qrc:/Sound/Sound/Wrong.mp3"));
        audioOutput->setVolume(0.5);
        player->play();


    QObject::connect(anim1, &QAbstractAnimation::finished, this, &MainWindow::updateQuestionsMethod);


}

//Most of this is not actually working since i dont wanna wait or do another animation
void MainWindow::rightAnswer()
{
    //Image Change

    score = 100 + (score * 1.2);

    ui->menuTEST->setTitle("Score: " + QString::number(score));


    ui->label->setText("CONGRATULATIONS");
    ui->label_2->setPixmap(QPixmap(":/Images/Images/Correct.png"));
    ui->pushButton->hide();
    ui->pushButton_2->hide();
    ui->pushButton_3->hide();
    ui->pushButton_4->hide();

    //wait x time and call
    updateQuestion();
}

void MainWindow::updateQuestionsMethod()
{
    updateQuestion();
}
