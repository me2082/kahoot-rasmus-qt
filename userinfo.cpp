#include "userinfo.h"
#include "mainwindow.h"
#include "ui_userinfo.h"
#include "QMovie"
#include <vector>

int turns = 0;
int lastPos = 0;
long totalNum = 0;
int wheelStrength = 2;
QString userName = "";

UserInfo::UserInfo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UserInfo)
{



    ui->setupUi(this);

    QMovie *movie = new QMovie("C:/Users/RBraun/Downloads/GigaBrainThink.gif");
    ui->label_4->setMovie(movie);
    movie->start();
}

UserInfo::~UserInfo()
{
    delete ui;
}


void UserInfo::on_NameDial_sliderMoved(int position)
{


    if(position == 0 && lastPos == 399)
    {
        turns++;
    }
    else if(position == 399 && lastPos == 0)
    {
        if(turns - 1 == -1)
        {
            turns = 0;
        }
        else
        {
            turns--;
        }
    }

    totalNum =  (399 * turns) + position;
    lastPos = position;

    int counter1 = 0;
    int counter2 = 0;
    int counter3 = 0;
    int counter4 = 0;
    int counter5 = 0;
    int counter6 = 0;
    int counter7 = 0;
    int counter8 = 0;
    int counter9 = 0;
    int counter10 = 0;
    int counter11 = 0;

    QString letter1;
    QString letter2;
    QString letter3;
    QString letter4;
    QString letter5;
    QString letter6;
    QString letter7;
    QString letter8;
    QString letter9;
    QString letter10;
    QString letter11;

    //0 Indexed
    QString lettersArray[27] = {"","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","Z","Y","Z"};

    //Divide by 0 exceptionhandling
    if(totalNum >= 27)
    {
        counter1 = (totalNum / 27);
        counter2 = (counter1/27);
        counter3 = (counter2/27);
        counter4 = (counter3/27);
        counter5 = (counter4/27);
        counter6 = (counter5/27);
        counter7 = (counter6/27);
        counter8 = (counter7/27);
        counter9 = (counter8/27);
        counter10 = (counter9/27);
        counter11 = (counter10/27);
    }

    letter1 =  lettersArray[totalNum - (counter1 * 27)];
    letter2 =  lettersArray[counter1 - (counter2 * 27)];
    letter3 =  lettersArray[counter2 - (counter3 * 27)];
    letter4 =  lettersArray[counter3 - (counter4 * 27)];
    letter5 =  lettersArray[counter4 - (counter5 * 27)];
    letter6 =  lettersArray[counter5 - (counter6 * 27)];
    letter7 =  lettersArray[counter6 - (counter7 * 27)];
    letter8 =  lettersArray[counter7 - (counter8 * 27)];
    letter9 =  lettersArray[counter8 - (counter9 * 27)];
    letter10 =  lettersArray[counter9 - (counter10 * 27)];
    letter11 =  lettersArray[counter10 - (counter11 * 27)];


    userName = letter11 + letter10 + letter9 + letter8 + letter7 + letter6 + letter5 + letter4 + letter3 + letter2 + letter1;
    ui->label->setText(userName);
}



void UserInfo::on_spinBox_valueChanged(int input)
{
    wheelStrength = input;

    ui->NameDial->setMaximum(wheelStrength);
}


void UserInfo::on_pushButton_clicked()
{
    mainwindow = new MainWindow(this, userName);

    mainwindow->show();

    this->hide();

}

