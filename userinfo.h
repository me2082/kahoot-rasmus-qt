#ifndef USERINFO_H
#define USERINFO_H

#include <QMainWindow>
#include "mainwindow.h"

namespace Ui {
class UserInfo;
}

class UserInfo : public QMainWindow
{
    Q_OBJECT

public:
    explicit UserInfo(QWidget *parent = nullptr);
    ~UserInfo();

private slots:
    void on_NameDial_sliderMoved(int position);

    void on_spinBox_valueChanged(int arg1);

    void on_pushButton_clicked();

private:
    Ui::UserInfo *ui;
    MainWindow* mainwindow= nullptr;
};

#endif // USERINFO_H
